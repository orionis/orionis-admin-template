$(function(){
	/**
	 * Messager
	 * 	范例: <button data-messager="{title:'您有新消息', content: '您有2条新消息'}" ></button>
	 */
	$("*[data-message-messager]").die("click").live("click",function(){
		var opt = eval("(" + $(this).attr("data-message-messager") + ")");
		$.orionis.message(opt.title, opt.content); 
	});
	$("*[data-message-alert]").click(function(){
		var opt = eval("(" + $(this).attr("data-message-alert") + ")");
		$.orionis.question(opt.title, opt.content);
		$.orionis.info(opt.title, opt.content);
		$.orionis.alert(opt.title, opt.content);
		$.orionis.warning(opt.title, opt.content);
		$.orionis.error(opt.title, opt.content);
	});
	$("*[data-message-confirm]").click(function(){
		var opt = eval("(" + $(this).attr("data-message-confirm") + ")");
		$.orionis.confirm(opt.title, opt.content, function(r){
			if(r){
				$.orionis.message("哈哈", "你好，这里是中国联通。。");
			}
		});
	});
	$("button[data-dialog]").click(function(){
		$.orionis.dialog({href: "dialog.html",title:"你好"});
	});
	$("button[data-window]").click(function(){
		$.orionis.window({href: "dialog.html", title:"哈哈"});
	});
});
