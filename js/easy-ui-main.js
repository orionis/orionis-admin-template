$(function() {
	/********************************************************
	 * 		页面总体控制
	 *******************************************************/
	// ---------------- 禁止没有链接地址的超链接 ------------- //
    $('a[href^=#]').click(function (e) {
        e.preventDefault();
    })
    // ------------- Bootstrap tooltips -------------//
    $("[data-toggle=tooltip]").tooltip ({});
    $(".tip").tooltip ({placement: 'top'});
    $(".tipR").tooltip ({placement: 'right'});
    $(".tipB").tooltip ({placement: 'bottom'});
    $(".tipL").tooltip ({placement: 'left'});
    
    // ---------------   美化的滚动条    ------------------- //
    $("#main").niceScroll();
    
    // ------------- 二级侧栏菜单  --------------- //
    $("#sidebar li.has-sub>a").die("click").live("click", function(e){
    	e.preventDefault();
    	var _this = $(this);
    	_this.siblings("ul").slideToggle("fast", function(){
    		if(_this.find("i").hasClass("icon-chevron-down")){
    			_this.find("i").removeClass("icon-chevron-down").addClass("icon-chevron-up");
    		}else{
    			_this.find("i").removeClass("icon-chevron-up").addClass("icon-chevron-down");
    		}
    	});
    });

	// ------------ widget ---------------- //
	if($(".widget").length){
		$(".widget .widget-title a.minimize").die("click").live("click", function(e){
			var _this = $(this);
			var _icon = _this.find("i");
			_this.parent(".widget-title").siblings(".widget-content").slideToggle("fast", function(){
				if(_icon.hasClass("icon-chevron-up")){
					_icon.removeClass("icon-chevron-up").addClass("icon-chevron-down");
				}else{
					_icon.removeClass("icon-chevron-down").addClass("icon-chevron-up");
				}
			});
		});
	}
});