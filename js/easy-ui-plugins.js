jQuery.orionis = {
	/**
	 * 提示消息，在右下角
	 * @param {Object} title
	 * @param {Object} message
	 * @param {Object} timeout
	 */
	message: function(title, message, timeout){
		// 设置默认超时时间
		if(arguments.length <= 2){
			timeout = 5000;
		}
		
		$.messager.show({  
                title:title,  
                msg:message,  
                timeout:timeout,  
                showType:'slide'  
            });
	},
	/**
	 * 普通提示消息
	 * @param {Object} title
	 * @param {Object} message
	 */
	alert: function(title, message){
		$.messager.alert(title,message);  
	},
	/**
	 * 错误提示消息
	 * @param {Object} title
	 * @param {Object} message
	 */
	error: function(title, message){
		$.messager.alert(title, message, 'error');
	},
	/**
	 * 信息提示
	 * @param {Object} title
	 * @param {Object} message
	 */
	info: function(title, message){
		$.messager.alert(title, message, 'info');
	},
	/**
	 * 问题提示
	 * @param {Object} title
	 * @param {Object} message
	 */
	question: function(title, message){
		$.messager.alert(title, message, 'question');
	},
	/**
	 * 警告消息
	 * @param {Object} title
	 * @param {Object} message
	 */
	warning: function(title, message){
		$.messager.alert(title, message, 'warning');
	},
	/**
	 * 确认消息
	 * @param {Object} title
	 * @param {Object} message
	 * @param {Object} callback
	 */
	confirm: function(title, content, callback){
		$.messager.confirm(title, content, callback);  
	},
	/**
	 * 提示输入信息
	 * @param {Object} title
	 * @param {Object} message
	 * @param {Object} callback
	 */
	prompt: function(title, content, callback){
		$.messager.prompt(title, content, callback);
	},
	/**
	 * 对话框
	 * @param {Object} _options
	 * 常用属性：
	 * 		title, width, height, href, closed, cache, modal
	 */
	dialog: function( _options){
		var options = $.extend({
				title: "对话框",  
			    width: 400,  
			    height: 200,  
			    closed: false,  
			    cache: false,  
			    modal: true  
			}, _options);
		
    	$("#dialog").dialog(options);
	},
	/**
	 * 窗口
     * @param {Object} _options
	 */
	window: function(_options){
		var options = $.extend({
				width:600,  
			    height:400,  
			    modal:true,
			    title:"窗口"
			}, _options);
		
		$("#window").window(options);
	}
};
