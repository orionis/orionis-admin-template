$(function() {
	/********************************************************
	 * 		页面总体控制
	 *******************************************************/
	// ---------------- 禁止没有链接地址的超链接 ------------- //
	$('a[href^=#]').click(function(e) {
		e.preventDefault();
	})
	// ------------- Bootstrap tooltips -------------//
	$("[data-toggle=tooltip]").tooltip({});
	$(".tip").tooltip({
		placement : 'top'
	});
	$(".tipR").tooltip({
		placement : 'right'
	});
	$(".tipB").tooltip({
		placement : 'bottom'
	});
	$(".tipL").tooltip({
		placement : 'left'
	});

	// ------------ 主页侧边栏收缩 --------------- //
	if ($("#sidebar .resize").length) {
		$("#sidebar .resize").click(function(e) {
			$("#sidebar").fadeToggle("fast", function() {
				$("#content").css("margin-left", 0);
			});
			$(".sidebar_small").fadeToggle("fast");
		});
		$(".sidebar_small").click(function(e) {
			$("#content").css("margin-left", 213);
			$(".sidebar_small").fadeToggle("fast", function() {
				$("#sidebar").fadeToggle("fast");
			});
		});
	}
	
	// ------------ widget ---------------- //
	if($(".widget").length){
		$(".widget .widget-title a.minimize").die("click").live("click", function(e){
			var _this = $(this);
			var _icon = _this.find("i");
			_this.parent(".widget-title").siblings(".widget-content").slideToggle("fast", function(){
				if(_icon.hasClass("icon-chevron-up")){
					_icon.removeClass("icon-chevron-up").addClass("icon-chevron-down");
				}else{
					_icon.removeClass("icon-chevron-down").addClass("icon-chevron-up");
				}
			});
		});
	}
});
