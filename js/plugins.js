// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Tabs
jQuery.o_tabs = {
	// 初始化Tabs
	initTabs: function(selector){
		var _tabs = $(selector).tabs();
		$.o_tabs.delegateClose(_tabs);
		return _tabs;
	},
	// 添加Tabs
	addTabs: function(_tabs, title, id, tabContent) {
		// 检查是否已经存在ID的Tab了，如果存在则获取焦点
		var _index = $.o_tabs.indexOf(_tabs, id);
		if(_index != -1){
			_tabs.tabs("option", "active", _index);
			return false;
		}
		// 创建新的Tab
		var _tabTemplate = "<li><a href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close' role='presentation'>关闭</span></li>";
		var li = $( _tabTemplate.replace( /#\{href\}/g, "#" + id ).replace( /#\{label\}/g, title ) );
		
		_tabs.find( ".ui-tabs-nav" ).append( li );
		_tabs.append( "<div id='" + id + "'><p>" + tabContent + "</p></div>" );
		_tabs.tabs( "refresh" );
		
		$.o_tabs.delegateClose(_tabs);
		$("a[href=#" + id + "]").trigger("click");
		
		return true;
	},
	// 为tab页添加关闭按钮
	delegateClose: function(_tabs){
		var closeBtn = "span.ui-icon-close";
		_tabs.undelegate(closeBtn, "click").delegate(closeBtn,"click", function(){
			var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
			_tabs.find(" #" + panelId ).remove();
			_tabs.tabs( "refresh" );
			_tabs.tabs("option", "active", $.o_tabs.count(_tabs));
		});
	},
	// 返回当前Tab页的总数
	count: function(_tabs){
		return _tabs.find(".ui-tabs-nav li").length - 1;
	},
	// 检查指定ID的Tab是否存在
	// 存在返回索引，否则返回-1
	indexOf: function(_tabs, _id){
		
		var index = -1;
		_tabs.find(".ui-tabs-nav li a").each(function(i){
			var id = $(this).attr("href").substring(1);
			if(id == _id){
				index = i;
				return false;
			}
		});
		
		return index;
	}
};
